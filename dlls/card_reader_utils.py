import logging
import ctypes
import os

logging.basicConfig(level=logging.DEBUG)


class CardReaderUtils:
    def __init__(self):
        os.environ['PATH'] = os.path.dirname(__file__) + ';' + os.environ['PATH']
        logging.debug('     [CardReaderUtils] READING DLL...')
        self.READER = ctypes.WinDLL("HIDReadCard.dll")
        self.set_language_english()

    def set_language_english(self):
        language = bytes("ENGLISH", 'utf-8')
        self.READER.ChangeDllLanguage(language)
        logging.debug('     [CardReaderUtils] Set language: ENGLISH')

    def get_error_message(self, code):
        get_msg_func = self.READER.GetMsg
        sz_prog_size = ctypes.create_string_buffer(500)
        mess = ctypes.c_char_p(ctypes.addressof(sz_prog_size))
        get_msg_func.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_char_p)]
        get_msg_func.restype = None
        get_msg_func(code, ctypes.byref(mess))
        return mess.value.decode('utf-8')

    def set_type_a(self):
        logging.debug('     [CardReaderUtils] SET TYPE A')
        return self.READER.USBSetTypeAOrB(0)

    def set_type_b(self):
        logging.debug('     [CardReaderUtils] SET TYPE B')
        return self.READER.USBSetTypeAOrB(1)

    def stop_card(self):
        logging.debug('     [CardReaderUtils] STOP CARD OPERATION')
        return self.READER.USBStopCard()

    def request_card(self):
        logging.debug('     [CardReaderUtils] REQUEST ALL CARD. PARAMS: 1, 170')
        self.READER.USBRequestCard.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_int)]
        self.READER.USBRequestCard.restype = ctypes.c_int
        tagtype = ctypes.c_int(170)
        return self.READER.USBRequestCard(1, ctypes.byref(tagtype))

    def prevent_collision(self):
        logging.debug('     [CardReaderUtils] PREVENT COLLISION')
        prevent_coll = self.READER.USBPreventionCollision
        sz_prog_size = ctypes.create_string_buffer(500)
        mess = ctypes.c_char_p(ctypes.addressof(sz_prog_size))
        prevent_coll.argtypes = [ctypes.POINTER(ctypes.c_char_p)]
        prevent_coll.restype = ctypes.c_int
        ret_code = prevent_coll(ctypes.byref(mess))
        if ret_code == 0:
            ret = {'status': True, 'card_serial_number': mess.value.decode('utf-8')}
        else:
            ret = {
                'status': False,
                'card_serial_number': '',
                'message': self.get_error_message(ret_code),
                'error_code': ret_code
            }
        return ret

    def choose_card(self, card_serial_number):
        logging.debug('     [CardReaderUtils - choose_card]CHOOSE CARD: ' + card_serial_number)
        the_funct = self.READER.USBChooseCard
        sz_prog_size = ctypes.c_int()
        card_serial_number_bin = ctypes.c_char_p(card_serial_number.encode('utf-8'))
        the_funct.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_int)]
        the_funct.restype = ctypes.c_int
        ret_code = the_funct(card_serial_number_bin, ctypes.byref(sz_prog_size))
        if ret_code == 0:  # success
            return {'status': True, 'size': sz_prog_size}
        else:
            return {
                'status': False,
                'size': 0,
                'error_code': ret_code,
                'message': self.get_error_message(ret_code)
            }

    def load_key(self, sector, password):  # password actually
        logging.debug('     [CardReaderUtils - load_key]' + str(sector) + ',' + password)
        return self.READER.USBLoadKey(0, sector, password.encode('utf-8'))

    def verify_key_a(self, sector):
        logging.debug('     [CardReaderUtils] verify key A at sector ' + str(sector))
        return self.READER.USBKeyAOrKeyB(0, sector)

    def verify_key_b(self, sector):
        logging.debug('     [CardReaderUtils] verify key B at sector ' + str(sector))
        return self.READER.USBKeyAOrKeyB(4, sector)

    def read_sector(self, sector):
        logging.debug('     [CardReaderUtils] Read data at sector: ' + str(sector))
        readsector_func = self.READER.USBReadSector
        sz_prog_size = ctypes.create_string_buffer(255)
        mess = ctypes.c_char_p(ctypes.addressof(sz_prog_size))
        readsector_func.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_char_p)]
        readsector_func.restype = ctypes.c_int
        ret_code = readsector_func(sector, ctypes.byref(mess))
        if ret_code == 0:
            ret = {'status': True, 'sector_data': mess.value.decode('utf-8')}
        else:
            ret = {
                'status': False,
                'sector_data': '',
                'message': self.get_error_message(ret_code),
                'error_code': ret_code
            }
            logging.debug('     [CardReaderUtils] Read data. Value is ' + ret['sector_data'])
        return ret

    def write_sector(self, sector, data):
        logging.debug('     [CardReaderUtils] write `' + data + '` to sector ' + str(sector * 4 + 3))
        return self.READER.USBWriteSector(sector * 4 + 3, data.encode('utf-8'))

    def play_sound(self, ms):
        logging.debug('     [CardReaderUtils] Play BEEP in ' + str(ms) + 'ms')
        return self.READER.USBSoundMS(ms)

    def turn_main_led_on(self):
        logging.debug('     [CardReaderUtils] turn on LED')
        return self.READER.USBLED(0, 1)

    def turn_main_led_off(self):
        logging.debug('     [CardReaderUtils] turn off LED')
        return self.READER.USBLED(0, 0)

    def read_card_sn(self):
        readsector_func = self.READER.GetCardSN
        sz_prog_size = ctypes.create_string_buffer(255)
        mess = ctypes.c_char_p(ctypes.addressof(sz_prog_size))
        readsector_func.argtypes = [ctypes.POINTER(ctypes.c_char_p), ctypes.c_bool]
        readsector_func.restype = ctypes.c_int
        ret_code = readsector_func(ctypes.byref(mess), True)
        if ret_code == 0:
            ret = {'status': True, 'serial_number': mess.value.decode('utf-8')}
        else:
            ret = {
                'status': False,
                'serial_number': '',
                'message': self.get_error_message(ret_code),
                'error_code': ret_code
            }
        logging.debug('     [CardReaderUtils] read card serial number: ' + ret['serial_number'])
        return ret
