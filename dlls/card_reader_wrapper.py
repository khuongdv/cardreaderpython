from . import CardReaderUtils
import json


class CardReaderWrapper:
    def __init__(self):
        self.cardReaderUtils = CardReaderUtils()

    def issue_card(self):
        print("Start issuing card...")
        self.cardReaderUtils.turn_main_led_on()
        check = self.cardReaderUtils.set_type_a()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.stop_card()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.request_card()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.prevent_collision()
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        card_serial_number = check['card_serial_number']

        check = self.cardReaderUtils.choose_card(card_serial_number)
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        check = self.cardReaderUtils.load_key(3, 'FFFFFFFFFFFF')
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.verify_key_a(3)
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.read_sector(3)
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        check = self.cardReaderUtils.write_sector(3, '000001F4FFFFFE0B000001F401FE01FE')
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)
        self.cardReaderUtils.turn_main_led_off()
        self.cardReaderUtils.play_sound(600)
        return json.dumps(
            {
                'card_serial_number': card_serial_number,
                'value': '000001F4FFFFFE0B000001F401FE01FE'
            }
        )

    def get_card_value(self):
        self.cardReaderUtils.turn_main_led_on()
        check = self.cardReaderUtils.set_type_a()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        self.cardReaderUtils.request_card()
        check = self.cardReaderUtils.prevent_collision()
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        card_serial_number = check['card_serial_number']

        check = self.cardReaderUtils.choose_card(card_serial_number)

        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']
        size = check['size']
        self.cardReaderUtils.turn_main_led_off()
        return self.cardReaderUtils.read_sector(1)

    def read_card_sn(self):
        return self.cardReaderUtils.read_card_sn()

    def turnOnLed(self):
        self.cardReaderUtils.turn_main_led_on()

    def turnOffLed(self):
        self.cardReaderUtils.turn_main_led_off()

    def sample_empty_card(self):
        print("Start sample_empty_card... ")
        self.cardReaderUtils.turn_main_led_on()
        check = self.cardReaderUtils.set_type_a()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.stop_card()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.request_card()
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.prevent_collision()
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        card_serial_number = check['card_serial_number']
        print('\t card serial number: ' + card_serial_number)

        check = self.cardReaderUtils.choose_card(card_serial_number)
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        check = self.cardReaderUtils.load_key(3, 'FFFFFFFFFFFF')
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.verify_key_a(3)
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)

        check = self.cardReaderUtils.read_sector(3)
        if not check['status']:
            self.cardReaderUtils.turn_main_led_off()
            return check['message']

        # check = self.cardReaderUtils.write_sector(0, '000001F4FFFFFE0B000001F401FE01FE')
        check = self.cardReaderUtils.write_sector(3, 'FFFFFFFFFFFF08778FFFFFFFFFFFFF')
        if check != 0:
            self.cardReaderUtils.turn_main_led_off()
            return self.cardReaderUtils.get_error_message(check)
        self.cardReaderUtils.turn_main_led_off()
        self.cardReaderUtils.play_sound(600)
        return json.dumps(
            {
                'card_serial_number': card_serial_number,
                'value': '000001F4FFFFFE0B000001F401FE01FE'
            }
        )